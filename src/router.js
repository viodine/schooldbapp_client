import Vue from "vue";
import Router from "vue-router";
import Students from "./views/Students.vue";
import Teachers from "./views/Teachers.vue";
import AddStudent from "./views/AddStudent.vue";
import EditStudent from "./views/EditStudent.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Students",
      component: Students,
      props: true
    },
    {
      path: "/students/new",
      name: "NewStudent",
      component: AddStudent
    },
    {
      path: "/students/:id",
      name: "EditStudent",
      component: EditStudent,
      props: true
    },
    {
      path: "/teachers",
      name: "Teachers",
      component: Teachers
    }
  ]
});
